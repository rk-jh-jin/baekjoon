import sys
sys.setrecursionlimit(111111)
input = sys.stdin.readline

T = int(input())

def dfs(start):
    global count
    visited[start] = True
    next = studentPick[start]
    if visited[next] == True:
        if finished[next] == False:
            tmp = next
            while True:
                count += 1
                tmp = studentPick[tmp]
                if tmp == next:
                    break
    else:
        dfs(next)
    finished[start] = True


for i in range(T):
    N = int(input())
    studentPick = list(map(int, input().split()))
    studentPick.insert(0, 0)
    count = 0
    finished = [False] * (N + 1)
    visited = [False] * (N + 1)
    for j in range(1, N + 1):
        if visited[j] == False:
            dfs(j)

    print(N - count)