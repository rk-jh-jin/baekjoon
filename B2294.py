N, target = map(int, input().split())
coinCase = [int(input()) for i in range(N)]

case = [0 for i in range(target + 1)]

for i in range(1, len(case)):
    cal = []
    for coin in coinCase:
        if coin <= i and case[i - coin] != -1:
            cal.append(case[i - coin])

    if not cal:
        case[i] = -1

    else:
        case[i] = min(cal) + 1
print(case[target])
