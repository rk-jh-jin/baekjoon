# 5
# 7
# 3 8
# 8 1 0
# 2 7 4 4
# 4 5 2 6 5
# (1, 1) = (0, 0) + (1, 1)
# (2, 1) = (2, 1) + (1, 0) or (2, 1) + (1, 1)
N = int(input())
tri = [list(map(int, input().split())) for i in range(N)]
for i in range(1, N):
    for j in range(len(tri[i])):
        if j == 0:
            tri[i][j] = tri[i][j] + tri[i - 1][j]
        elif j == (len(tri[i]) - 1):
            tri[i][j] = tri[i][j] + tri[i - 1][j - 1]
        else:
            tri[i][j] = tri[i][j] + max(tri[i - 1][j - 1], tri[i - 1][j])
print(max(tri[N - 1][k] for k in range(N)))
