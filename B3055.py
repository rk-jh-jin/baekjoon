H, W = map(int, input().split())
field = [list(input()) for i in range(H)]
moves = [[-1, 0], [1, 0], [0, 1], [0, -1]]

def check(field):
    start = ''
    water = []
    for i in range(H):
        for j in range(W):
            if field[i][j] == 'S':
                start = (i, j)
            elif field[i][j] == '*':
                water.append((i, j))
    
    return start, water

def bfs(field, start, water):
    stack = [start]
    count = 0
    while stack:
        for i in range(len(water)):
            a, b = water.pop()
            for move in moves:
                new_a, new_b = a + move[0], b + move[1]
                if 0 <= new_a < H and 0 <= new_b < W:
                    if field[new_a][new_b] == '.' or field[new_a][new_b] == 'S' or field[new_a][new_b] == 'P':
                        water.insert(0, (new_a, new_b))
                        field[new_a][new_b] = '*'

        for j in range(len(stack)):
            x, y = stack.pop()
            for move in moves:
                new_x, new_y = x + move[0], y + move[1]
                if 0 <= new_x < H and 0 <= new_y < W:
                    if field[new_x][new_y] == 'D':
                        return count + 1
                        
                    elif field[new_x][new_y] == '.':
                        stack.insert(0, (new_x, new_y))
                        field[new_x][new_y] = 'P'

        count += 1


    return 'KAKTUS'

start, water = check(field)
print(bfs(field, start, water))