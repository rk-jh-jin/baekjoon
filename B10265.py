# 싸이클 ~ 한 컴포넌트
# 낵샙으로 최대값
# 한 컴포넌트의 최소 길이(싸이클) ~ 컴포넌트 최대 길이
N, K = map(int, input().split())
graph = [0] + list(map(int, input().split()))

def dfs(graph, start):
    global cycle
    visited = [start]
    next = graph[start]
    while True:
        if next in visited:
            break

        visited.append(next)
        next = graph[next]
    if start == next:
        visited.sort()
        if visited not in cycle:
            cycle.append(visited)
    else:
        return visited

cycle = []
temp = []
for i in range(1, N + 1):
    tmp = dfs(graph, i)
    if tmp:
        temp.append(tmp)

print(cycle)

m = len(cycle)
print(temp)
result = [set(i) for i in cycle]
for i in range(len(temp)):
    B = set(temp[i])
    for j in range(m):
        A = set(cycle[j])
        if B - A != B:
            result[j] = result[j].union(B)
            break

case = []
for i in range(m):
    case.append((len(cycle[i]), len(result[i])))

print(case)
