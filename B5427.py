import sys
input = sys.stdin.readline

T = int(input())
moves = [[1, 0], [-1, 0], [0, 1], [0, -1]]

def checkStart(field):
    start = (0, 0)
    fire = []
    for i in range(H):
        for j in range(W):
            if field[i][j] == '@':
                start = (i, j)
            if field[i][j] == '*':
                fire.append((i, j))
    return start, fire

def bfs(field, start, fire):
    stack = [start]
    count = 1
    while stack:
        for i in range(len(fire)):
            a, b = fire.pop()
            for move in moves:
                new_a, new_b = a + move[0], b + move[1]
                if 0 <= new_a < H and 0 <= new_b < W:
                    if field[new_a][new_b] == '.' or field[new_a][new_b] == '@' or field[new_a][new_b] == 'P':
                        fire.insert(0, (new_a, new_b))
                        field[new_a][new_b] = 'X'

        for j in range(len(stack)):
            x, y = stack.pop()
            if x == H - 1 or x == 0 or y == W - 1 or y == 0:
                return count

            for move in moves:
                new_x, new_y = x + move[0], y + move[1]
                if 0 <= new_x < H and 0 <= new_y < W and field[new_x][new_y] == '.':
                    stack.insert(0, (new_x, new_y))
                    field[new_x][new_y] = 'P'
        count += 1
    
    return 'IMPOSSIBLE'

for i in range(T):
    W, H = map(int, input().split())
    field = [list(input()) for i in range(H)]
    if W == 1 and H == 1:
        print(1)
    else:
        start, fire = checkStart(field)
        print(bfs(field, start, fire))