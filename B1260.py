from collections import deque
N, M, V = map(int, input().split())
graph = {i : set() for i in range(1, N + 1)}
for i in range(M):
    x, y = map(int, input().split())
    graph[x].add(y)
    graph[y].add(x)

def dfs(graph, start):
    visited = []
    stack = [start]
    while stack:
        n = stack.pop()
        if n not in visited:
            visited.append(n)
            temp = list(graph[n] - set(visited))
            temp.sort(reverse=True)
            stack += temp
    return " ".join(list(map(str, visited)))

def bfs(graph, start):
    visited = []
    dq = deque([start])
    while dq:
        n = dq.popleft()
        if n not in visited:
            visited.append(n)
            tmp = list(graph[n] - set(visited))
            tmp.sort()
            dq += tmp

    return " ".join(list(map(str, visited)))

print(dfs(graph, V))
print(bfs(graph, V))
    