N, W = map(int, input().split())

# weight = []
# valid = []

# for i in range(N):
#     x, y = map(int, input().split())
#     weight.append(x)
#     valid.append(y)

# knap = [[0 for i in range(W + 1)] for j in range(N + 1)]

# for i in range(N + 1):
#     for j in range(W + 1):
#         if weight[i - 1] <= j:
#             knap[i][j] = max(knap[i - 1][j], valid[i - 1] + knap[i - 1][j - weight[i - 1]])
#         else:
#             knap[i][j] = knap[i - 1][j]

# print(knap)

case = [tuple(map(int, input().split())) for i in range(N)]
print(case)
knap = [0 for i in range(W + 1)]

for i in range(N):
    for j in range(W, 1, -1):
        if case[i][0] <= j:
            knap[j] = max(knap[j], case[i][1] + knap[j - case[i][0]])

print(max(knap))
