N = int(input())
price = list(map(int, input().split()))
price.insert(0, 0)
case = [0 for i in range(N + 1)]


for i in range(1, N + 1):
    cal = []
    for j in range(N + 1):
        if j > i:
            break

        cal.append(price[i - j] + case[j])

    case[i] = max(cal)

print(case[N])