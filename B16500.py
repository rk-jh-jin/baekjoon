target = input()
N = int(input())
case = [input() for i in range(N)]

for i in case:
    flag = 0
    if i in target:
        S = target.replace(i, "", 1)
        while True:
            temp = S
            if S == "":
                flag = 1
                break

            for j in case:
                if len(j) > len(S):
                    continue

                if j in S:
                    S = S.replace(j, "", 1)
            
            if temp == S:
                break

    if flag == 1:
        break

print(flag)