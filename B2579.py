# 6
# 1 10 
# 2 20
# 3 15
# 4 25
# 5 10
# 6 20
# [10, 20, 15, 25, 10, 20]

N = int(input())
case = [int(input()) for _ in range(N)]
result = []
result.append(case[0])
result.append(case[0] + case[1])
result.append(max(case[0] + case[2], case[1] + case[2]))
for i in range(3, N):
    result.append(max(result[i - 3] + case[i] + case[i - 1], case[i] + result[i - 2]))

print(result[N - 1])