import copy
N = int(input())
field = [list(input()) for i in range(N)]
newField = copy.deepcopy(field)
moves = [[-1, 0], [1, 0], [0, 1], [0, -1]]

def normal(field, i, j):
    tmp = field[i][j]
    dq = [(i, j)]
    while dq:
        x, y = dq.pop()
        field[x][y] = 0
        for move in moves:
            new_x, new_y = x + move[0], y + move[1]
            if 0 <= new_x < N and 0 <= new_y < N and field[new_x][new_y] == tmp:
                dq.insert(0, (new_x, new_y))
                field[new_x][new_y] = 0

    return 1

def abnormal(field, i, j):
    tmp = newField[i][j]
    if tmp == 'R' or tmp == 'G':
        tmp = ['R', 'G']
    else:
        tmp = ['B']
    dq = [(i, j)]
    while dq:
        x, y = dq.pop()
        newField[i][j] = 0
        for move in moves:
            new_x, new_y = x + move[0], y + move[1]
            if 0 <= new_x < N and 0 <= new_y < N and field[new_x][new_y] in tmp:
                dq.insert(0, (new_x, new_y))
                field[new_x][new_y] = 0
    return 1

normalCount = 0
abnormalCount = 0
for i in range(N):
    for j in range(N):
        if field[i][j] != 0:
            normalCount += normal(field, i, j)
        if newField[i][j] != 0:
            abnormalCount += abnormal(newField, i, j)

print(normalCount, abnormalCount)