N = int(input())

for i in range(N):
    n = int(input())
    testCase = [[int(i) for i in input().split()] for i in range(2)]

    testCase[0][1] += testCase[1][0]
    testCase[1][1] += testCase[0][0]

    for j in range(2, n):
        testCase[0][j] += max(testCase[1][j - 1], testCase[1][j - 2])
        testCase[1][j] += max(testCase[0][j - 1], testCase[0][j - 2])

    print(max(testCase[0][n - 1], testCase[1][n - 1]))