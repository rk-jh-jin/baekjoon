height, width, N = map(int, input().split())
field = [[0] * (width + 1) for i in range(height + 1)]

for i in range(N):
    x, y = map(int, input().split())
    field[x][y] = 1


moves = [[-1, 0], [1, 0], [0, 1], [0, -1]]

def dfs(field, start):
    count = 1
    check = [start]
    while check:
        x, y = check.pop()
        field[x][y] = 0
        for move in moves:
            new_x, new_y = x + move[0], y + move[1]
            if 0 < new_x <= height and 0 < new_y <= width and field[new_x][new_y] == 1:
                check.insert(0, (new_x, new_y))
                field[new_x][new_y] = 0
                count += 1

    return count

result = 0

for i in range(1, height + 1):
    for j in range(1, width + 1):
        if field[i][j] == 1:
            result = max(result, dfs(field,(i, j)))

print(result)