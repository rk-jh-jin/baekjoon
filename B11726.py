N = int(input())

first = 1
second = 2
temp = 0
for i in range(N - 1):
    temp = first
    first = second
    second = (first + temp) % 10007

print(first)