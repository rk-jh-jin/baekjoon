import sys
input = sys.stdin.readline

P, L = map(int, input().split())

graph = {i + 1 : set() for i in range(P)}
for i in range(L):
    u, v = map(int, input().split())
    graph[u].add(v)
    graph[v].add(u)

def dfs(graph, start):
    check[start] = True
    visited = []
    stack = [start]
    while stack:
        n = stack.pop()
        if n not in visited:
            check[n] = True
            visited.append(n)
            stack += graph[n] - set(visited)
            

count = 0
check = [False] * (P + 1)

for i in range(1, P + 1):
    if check[i] == False:
        dfs(graph, i)
        count += 1

print(count)