N = int(input())
#numberOfFile = 6, target = 0
#imp = [1, 1, 9, 1, 1, 1]
#imp.reverse() = [1, 1, 1, 9, 1, 1]

for i in range(N):
    numberOfFile, target = map(int, input().split())
    imp = list(map(int, input().split()))
    for j in range(numberOfFile):
        if j == target:
            imp[j] = (imp[j], 'x')
        else:
            imp[j] = (imp[j], 'y')
    imp.reverse()
    count = 1

    while True:
        if imp[-1][0] == max(imp)[0]:
            if imp[-1][1] == 'x':
                print(count)
                break
            else:
                imp.pop()
                count += 1
        else:
            tmp = imp.pop()
            imp.insert(0, tmp)