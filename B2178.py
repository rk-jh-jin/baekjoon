import sys
input = sys.stdin.readline
N, M = map(int, input().split())
maze = [list(input()) for i in range(N)]
moves = [[-1, 0], [1, 0], [0, 1], [0, -1]]

def bfs(field, start):
    stack = [start]
    count = 0
    while stack:
        for i in range(len(stack)):
            x, y = stack.pop()
            field[x][y] = 0
            for move in moves:
                new_x, new_y = x + move[0], y + move[1]
                if new_x == N - 1 and new_y == M - 1:
                    return (count + 2)
                if 0 <= new_x < N and 0 <= new_y < M and field[new_x][new_y] == '1':
                    stack.insert(0, (new_x, new_y))
                    field[new_x][new_y] = 0
        count += 1
    return count

print(bfs(maze, (0, 0)))