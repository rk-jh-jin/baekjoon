from itertools import permutations
case = list(permutations(range(1,10), 3))

N = int(input())
numList = [tuple(map(int, input().split())) for i in range(N)]

result = 0

def check(x, y, z, num, strike, ball):
    countOfStrike = 0
    countOfBall = 0
    if x == num[0]:
        countOfStrike += 1
    
    if y == num[1]:
        countOfStrike += 1

    if z == num[2]:
        countOfStrike += 1

    if x in num and num.index(x) != 0:
        countOfBall += 1

    if y in num and num.index(y) != 1:
        countOfBall += 1
    
    if z in num and num.index(z) != 2:
        countOfBall += 1

    if countOfStrike == strike:
        if countOfBall == ball:
            return True
    
    return False


for sample in case:
    x, y, z = sample
    for i in numList:
        num, strike, ball = i
        num = list(map(int, list(str(num))))
        save = 1
        if check(x, y, z, num, strike, ball) == False:
            save = 0
            break

    if save != 0:
        result += 1
        

print(result)

