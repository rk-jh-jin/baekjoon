import sys
import copy
input = sys.stdin.readline
N, M = map(int, input().split())
field = [list(input()) for i in range(N)]
field = [field, copy.deepcopy(field)]
moves = [[-1, 0], [1, 0], [0, 1], [0, -1]]

def bfs(field, start):
    stack = [start]
    count = 1
    while stack:
        for i in range(len(stack)):
            C, x, y = stack.pop()
            field[C][x][y] = 'P'
            for move in moves:
                new_x, new_y = x + move[0], y + move[1]
                if new_x == N - 1 and new_y == M - 1:
                    return count + 1

                if 0 <= new_x < N and 0 <= new_y < M:
                    if C == 0:
                        if field[C][new_x][new_y] == '1':
                            field[1][new_x][new_y] = 'P'
                            stack.insert(0, (1, new_x, new_y))
                        elif field[C][new_x][new_y] == '0':
                            field[C][new_x][new_y] = 'P'
                            stack.insert(0, (0, new_x, new_y))
                    else:
                        if field[C][new_x][new_y] == '0':
                            field[C][new_x][new_y] = 'P'
                            stack.insert(0, (1, new_x, new_y))
        count += 1
    return - 1
if N == 1 and M == 1:
    print(1)
else:
    print(bfs(field, (0, 0, 0)))
