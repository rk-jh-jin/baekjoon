moves = [[1, 0, 0], [-1, 0, 0], [0, 1, 0], [0, -1, 0], [0, 0, 1], [0, 0, -1]]


def bfs(field, start):
    stack = [start]
    count = 0
    while stack:
        for i in range(len(stack)):
            z, x, y = stack.pop()
            for move in moves:
                new_z, new_x, new_y = z + move[0], x + move[1], y + move[2]
                if 0 <= new_z < N and 0 <= new_x < I and 0 <= new_y < J:
                    if field[new_z][new_x][new_y] == 'E':
                        return count + 1
                    elif field[new_z][new_x][new_y] == '.':
                        stack.insert(0, (new_z, new_x, new_y))
                        field[new_z][new_x][new_y] = 0
        count += 1
    return "Trapped!"

def checkStart(field):
    for z in range(N):
        for i in range(I):
            for j in range(J):
                if field[z][i][j] == 'S':
                    return (z, i, j)
while True:
    N, I, J = map(int, input().split())

    if N == 0:
        break

    field = []
    for j in range(N):
        tmp = [list(input()) for i in range(I)]
        field.append(tmp)
        input()

    sp = checkStart(field)

    result = bfs(field, sp)

    if type(result) == str:
        print(result)
    else:
        print("Escaped in %d minute(s)." %result)