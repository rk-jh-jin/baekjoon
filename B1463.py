# N = int(input())

# result = [N]
# count = 0

# while True:
#     if 1 in result:
#         print(count)
#         break

#     case = []

#     for i in result:
#         if i % 3 == 0:
#             case.append(i // 3)

#         if i % 2 == 0:
#             case.append(i // 2)

#         case.append(i - 1)

#     result = case
#     count += 1

N = int(input())


def solution(N):
    count = 0
    if N == 1:
        return count

    count = solution(N - 1) + 1
    if N % 3 == 0:
        count = min(count, solution(N // 3) + 1)
    if N % 2 == 0:
        count = min(count, solution(N // 2) + 1)

    return count

print(solution(N))