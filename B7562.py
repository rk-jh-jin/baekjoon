C = int(input())
moves = [[-2, 1], [-1, 2], [1, 2], [2, 1], [2, -1], [1, -2], [-1, -2], [-2, -1]]

def bfs(field, start, end):
    stack = [start]
    count = 0
    while stack:
        if end in stack:
            return count
        for i in range(len(stack)):
            x, y = stack.pop()
            field[x][y] = 1
            for move in moves:
                new_x, new_y = x + move[0], y + move[1]
                if 0 <= new_x < L and 0 <= new_y < L and field[new_x][new_y] == 0:
                    stack.insert(0, (new_x, new_y))
                    field[new_x][new_y] = 1
        count += 1



for i in range(C):
    L = int(input())
    field = [[0] * L for k in range(L)]
    start = tuple(map(int, input().split()))
    end = tuple(map(int, input().split()))
    print(bfs(field, start, end))