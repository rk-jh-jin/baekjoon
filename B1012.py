from collections import deque
import sys
input = sys.stdin.readline
moves = [(-1,0), (1,0), (0,-1), (0,1)]

def bfs(graph, start):
    dq = deque()
    dq.append(start)
    while dq:
        y, x = dq.popleft()
        graph[y][x] = 0
        for move in moves:
            new_y, new_x = y + move[0], x + move[1]
            if 0 <= new_y < height and 0 <= new_x < width and graph[new_y][new_x]:
                dq.append((new_y, new_x))
                graph[new_y][new_x] = 0

    return 1

T = int(input())

for i in range(T):
    # input 받아서 field 생성
    width, height, N = map(int, input().split())
    field = [[0 for i in range(width)] for j in range(height)]

    if width * height == N:
        print(1)
        continue

    count = 0

    # field 채우기
    for j in range(N):
        x, y = map(int, input().split())
        field[y][x] = 1
    
    # field 탐색
    for x in range(width):
        for y in range(height):
            if field[y][x] == 1:
               count += bfs(field, (y, x))
               
    print(count)