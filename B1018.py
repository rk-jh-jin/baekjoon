def check(newCase):
    global result
    count1 = 0
    count2 = 0
    for i in range(0,8,2):
        for j in range(0,8,2):
            if newCase[i][j] != 'B':
                count1 += 1
            if newCase[i][j+1] != 'W':
                count1 += 1
            if newCase[i+1][j] != 'W':
                count1 += 1
            if newCase[i+1][j+1] != 'B':
                count1 += 1

            if newCase[i][j] != 'W':
                count2 += 1
            if newCase[i][j+1] != 'B':
                count2 += 1
            if newCase[i+1][j] != 'B':
                count2 += 1
            if newCase[i+1][j+1] != 'W':
                count2 += 1
    
            
    result = min(result, count1, count2)

M, N = map(int, input().split())
case = [[i for i in input()] for j in range(M)]

row, col = 0, 0

result = 32

while row < M - 7:
    newCase = []
    for i in range(row, row + 8):
        newCase.append(case[i][col:col+8])

    check(newCase)

    col += 1

    if col == N - 7:
        row += 1
        col = 0
    

print(result)