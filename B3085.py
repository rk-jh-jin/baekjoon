N = int(input())
candySet = [[x for x in input()] for i in range(N)]
candySetReverse = list(map(list, zip(*candySet)))

result = 0

def check(candySet, i, j, N):
    global result
    count = 1
    # 행
    for y in range(N - 1):
        save = candySet[i][y]
        if save == candySet[i][y + 1]:
            count += 1
            result = max(result, count)
        else:
            result = max(result, count)
            count = 1
    
    # 열1
    count = 1
    for x in range(N - 1):
        save = candySet[x][j]
        if save == candySet[x + 1][j]:
            count += 1
            result = max(result, count)
        else:
            result = max(result, count)
            count = 1

    # 열2
    count = 1
    for x in range(N - 1):
        save = candySet[x][j + 1]
        if save == candySet[x + 1][j + 1]:
            count += 1
            result = max(result, count)
        else:
            result = max(result, count)
            count = 1

            
    
def changeCandy(candySet, N):
    for i in range(N):
        j = 0
        while j < N - 1:
            candySet[i][j], candySet[i][j + 1] = candySet[i][j + 1], candySet[i][j]
            check(candySet, i, j, N)
            candySet[i][j], candySet[i][j + 1] = candySet[i][j + 1], candySet[i][j]
            j += 1

changeCandy(candySet, N)
changeCandy(candySetReverse, N)

print(result)