N = int(input())

num1 = 1
num2 = 1
for i in range(1, N):
    result = (num1 + num2) % 15746
    num2 = num1
    num1 = result

print(result)