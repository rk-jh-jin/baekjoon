N = int(input())
case = list(map(int, input().split()))
result = [0 for i in range(N)]

for idx, num in enumerate(case):
    temp = []
    for x, y in enumerate(case[:idx + 1]):
        if y < num:
            temp.append(x)
    
    if temp == []:
        result[idx] = num
    else:
        cal = 0
        for j in temp:
            cal = max(cal, result[j])
        result[idx] = cal + num
    
print(max(result))