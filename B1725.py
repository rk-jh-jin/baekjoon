N = int(input())
height = [int(input()) for i in range(N)]

stack = []
result = 0

# for i in range(N):
#     if stack == [] or height[stack[-1]] < height[i]:
#         stack.append(i)
#     else:
#         while True:
#             h = stack.pop()
#             if stack == []:
#                 result = max(result, height[h] * i)
#                 break
#             else:
#                 result = max(result, height[h] * (i - 1 - stack[-1]))
            
#             if height[stack[-1]] < height[i]:
#                 break

#         stack.append(i)


# while stack != []:
#     h = stack.pop()
#     if stack != []:
#         result = max(result, height[h] * (N - 1 - stack[-1]))
#     else:
#         result = max(result, height[h] * N)

# print(result)

for i in range(N):
    while stack != [] and height[stack[-1]] > height[i]:
        h = stack[-1]
        stack.pop()
        w = i
        if stack != []:
            w -= stack[-1] + 1
        result = max(result, height[h] * w)

    stack.append(i)

while stack != []:
    h = stack[-1]
    stack.pop()
    w = N
    if stack != []:
        w -= stack[-1] + 1
    result = max(result, height[h] * w)

print(result)