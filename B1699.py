N = int(input())
case = [0 for i in range(N + 1)]
squareSet = [i * i for i in range(1, 317)]

for i in range(1, N + 1):
    temp = []
    for j in squareSet:
        if j > i:
            break

        temp.append(case[i - j])
    print(temp)
    case[i] = min(temp) + 1

print(case[N])