F, start, goal, U, D = map(int, input().split())
moves = [U, -D]
building  = [0 for i in range(0, F + 1)]

def bfs(building, start, end):
    stack = [start]
    count = 0
    while stack:
        if end in stack:
            return count
        for i in range(len(stack)):
            cur = stack.pop()
            building[cur] = 1
            for move in moves:
                next = cur + move
                if 1 <= next <= F and next != cur and building[next] == 0:
                    stack.insert(0, next)
        
        count += 1
        stack = list(set(stack))

    return "use the stairs"

print(bfs(building, start, goal))