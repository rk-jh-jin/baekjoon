N = int(input())
field = [list(input()) for i in range(N)]
moves = [[-1, 0], [1, 0], [0, 1], [0, -1]]

result = []

def dfs(field, start):
    count = 1
    dq = [start]
    while dq:
        x, y = dq.pop()
        field[x][y] = 0
        for move in moves:
            new_x, new_y = x + move[0], y + move[1]
            if 0 <= new_x < N and 0 <= new_y < N and field[new_x][new_y] == '1':
                dq.insert(0, (new_x, new_y))
                field[new_x][new_y] = 0
                count += 1
    return count

for i in range(N):
    for j in range(N):
        if field[i][j] == '1':
            result.append(dfs(field, (i, j)))

print(len(result))
result.sort()
for i in result:
    print(i)