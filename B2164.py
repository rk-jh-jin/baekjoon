N = int(input())

card = [x for x in range(1, N + 1)]

while len(card) > 1:
    print(card)
    if len(card) % 2:
        tmp = card.pop()
        card = card[1::2]
        card.insert(0, tmp)
    else:
        card = card[1::2]

print(card[0])