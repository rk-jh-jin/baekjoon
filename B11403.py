N = int(input())
graph = {}
for i in range(N):
    sample = list(map(int, input().split()))
    tmp = []
    for j in range(N):
        if sample[j] == 1:
            tmp.append(j + 1)
    graph[i + 1] = set(tmp)

def dfs(graph, start):
    visited = []
    stack = [start]

    n = stack.pop()
    if n not in visited:
        stack += graph[n] - set(visited)

    while stack:
        n = stack.pop()
        if n not in visited:
            visited.append(n)
            stack += graph[n] - set(visited)
    return visited

result = [[0] * N for i in range(N)]

for i in range(N):
    tmp = dfs(graph, i + 1) #[1, 2, 3]
    for j in range(N):
        if j + 1 in tmp:
            result[i][j] = 1

for i in range(N):
    print(" ".join(list(map(str, result[i]))))
