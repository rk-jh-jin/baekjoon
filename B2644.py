from collections import deque
N = int(input())
target1, target2 = map(int, input().split())
M = int(input())
graph = {i : set() for i in range(1, N + 1)}

for i in range(M):
    x, y = map(int, input().split())
    graph[x].add(y)
    graph[y].add(x)


def bfs(graph, start, end):
    count = 0
    visited = []
    dq = deque([start])
    while dq:
        tmp = []
        k = len(dq)
        for i in range(k):
            n = dq.popleft()
            if n not in visited:
                visited.append(n)
                tmp += graph[n] - set(visited)
        dq += tmp
        count += 1
        if end in tmp:
            return count

    return -1

print(bfs(graph, target1, target2))