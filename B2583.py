M, N, K = map(int, input().split())
field = [[0] * N for i in range(M)]
moves = [[-1, 0], [1, 0], [0, 1], [0, -1]]

for i in range(K):
    a, b, c, d = map(int, input().split())
    # [M - b - 1][a], [M - d][c - 1]
    for x in range(M - d, M - b):
        for y in range(a, c):
            field[x][y] = 1
        
def dfs(field, start):
    count = 1
    dq = [start]
    while dq:
        x, y = dq.pop()
        field[x][y] = 1
        for move in moves:
            new_x, new_y = x + move[0], y + move[1]
            if 0 <= new_x < M and 0 <= new_y < N and field[new_x][new_y] == 0:
                dq.insert(0, (new_x, new_y))
                field[new_x][new_y] = 1
                count += 1

    return count

result = []
for i in range(M):
    for j in range(N):
        if field[i][j] == 0:
            result.append(dfs(field, (i, j)))

print(len(result))
result.sort()
result = list(map(str, result))
print(" ".join(result))