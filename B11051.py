N, K = map(int, input().split())

up = 1
down = 1

for i in range(N, N - K, -1):
    up *= i

for j in range(1, K + 1):
    down *= j

print((up // down) % 10007)