import sys
input = sys.stdin.readline

N = int(input())
qu = []

for i in range(N):
    tmp = input().split()
    if tmp[0] == 'push':
        qu.insert(0, int(tmp[1]))
    elif tmp[0] == 'pop':
        if qu != []:
            print(qu.pop())
        else:
            print(-1)
    elif tmp[0] == 'size':
        print(len(qu))
    elif tmp[0] == 'empty':
        if qu == []:
            print(1)
        else:
            print(0)
    elif tmp[0] == 'front':
        if qu != []:
            print(qu[-1])
        else:
            print(-1)
    elif tmp[0] == 'back':
        if qu != []:
            print(qu[0])
        else:
            print(-1)
