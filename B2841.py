import sys
input = sys.stdin.readline
N, maxFret = map(int, input().split())

count = 0
stack = [[], [], [], [], [], []]

for i in range(N):
    line, fret = map(int, input().split())
    if stack[line - 1] == [] or stack[line - 1][-1] < fret:
        stack[line - 1].append(fret)
        count += 1
    elif stack[line - 1][-1] == fret:
        continue
    elif stack[line - 1][-1] > fret:
        while True:
            if stack[line - 1] == [] or stack[line - 1][-1] <= fret:
                break
            stack[line - 1].pop()
            count += 1
        if stack[line - 1] != [] and stack[line - 1][-1] == fret:
            continue
        else:
            stack[line - 1].append(fret)
            count += 1
        
print(count)