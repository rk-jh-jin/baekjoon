N = int(input())

count = 0

for i in range(N):
    string = input()
    stack = []
    for j in string:
        if stack == [] or stack[-1] != j:
            stack.append(j)
        else:
            stack.pop()
    
    if stack == []:
        count += 1

print(count)