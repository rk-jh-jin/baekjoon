# 10
# 10 -4 3 1 5 6 -35 12 21 -1
N = int(input())
numList = list(map(int, input().split()))
cursum = 0
maxsum = 0

for i in numList:
    cursum += i
    if cursum < 0:
        cursum = 0
    maxsum = max(maxsum, cursum)

if max(numList) <= 0:
    maxsum = max(numList)

print(maxsum)