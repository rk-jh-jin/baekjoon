dwarfSet = [int(input()) for i in range(9)]

N = sum(dwarfSet) - 100
case = []
for num in dwarfSet:
    if (N - num) in dwarfSet and (N - num) != num:
        case += [num, N - num]
        break

result = [x for x in dwarfSet if x not in case]
result.sort()
for i in result:
    print(i)