import sys
input = sys.stdin.readline
N = int(input())
dq = []

for i in range(N):
    tc = input().split()
    if tc[0] == 'push_front':
        dq.insert(0, int(tc[1]))
    elif tc[0] == 'push_back':
        dq.append(int(tc[1]))
    elif tc[0] == 'pop_front':
        if dq != []:
            print(dq[0])
            dq.remove(dq[0])
        else:
            print(-1)
    elif tc[0] == 'pop_back':
        if dq != []:
            tmp = dq.pop()
            print(tmp)
        else:
            print(-1)
    elif tc[0] == 'size':
        print(len(dq))
    elif tc[0] == 'empty':
        if dq != []:
            print(0)
        else:
            print(1)
    elif tc[0] == 'front':
        if dq != []:
            print(dq[0])
        else:
            print(-1)
    elif tc[0] == 'back':
        if dq != []:
            print(dq[-1])
        else:
            print(-1)