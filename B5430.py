import ast
import sys
input = sys.stdin.readline
T = int(input())

for i in range(T):
    order = input()
    N = int(input())
    numarr = ast.literal_eval(input())
    stack = []
    if N == 0 or N < order.count('D') or numarr == []:
        print('error')
        continue

    elif N == order.count('D'):
        print([])
        continue
    
    for j in order:
        if j == 'R':
            numarr.reverse()
        elif j == 'D':
            numarr.remove(numarr[0])

    print(numarr)