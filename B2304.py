N = int(input())
pillar = [tuple(map(int, input().split())) for i in range(N)]
pillar.sort()

stack = []
result = 0
maxHeight = max([y for (x, y) in pillar])

for i in range(N):
    pos, height = pillar[i]

    if height == maxHeight:
        left = pos
        if stack != []:
            tmp = stack.pop()
            result += abs(pos - tmp[0]) * tmp[1]
        break

    if stack == []:
        stack.append(pillar[i])
    elif stack[-1][1] < height:
        tmp = stack.pop()
        result += abs(pos - tmp[0]) * tmp[1]
        stack.append(pillar[i])
    else:
        continue

stack = [] 
for j in range(N - 1, -1, -1):
    pos, height = pillar[j]

    if height == maxHeight:
        right = pos
        if stack != []:
            tmp = stack.pop()
            result += abs(pos - tmp[0]) * tmp[1]
        break

    if stack == []:
        stack.append(pillar[j])
    elif stack[-1][1] < height:
        tmp = stack.pop()
        result += abs(pos - tmp[0]) * tmp[1]
        stack.append(pillar[j])
    else:
        continue

result += (right - left + 1) * maxHeight

print(result)