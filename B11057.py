N = int(input())
case = [[1 for i in range(10)] for i in range(N)]

for i in range(1, N):
    for j in range(1, 10):
        case[i][j] = sum(case[i - 1][:j + 1])

print(sum(case[N - 1]) % 10007)