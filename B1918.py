S = input()
priority = {'*' : 2, '/' : 2, '+' : 1, '-' : 1, '(' : 0, ')' : 0}
stack = []
result = ""
for i in '(' + S + ')':
    if i not in priority:
        result += i
    elif i == '(':
        stack.append(i)
    elif i == ')':
        while True:
            if stack[-1] == '(':
                stack.pop()
                break
            result += stack.pop()
    else:
        while stack[-1] != '(' and priority[i] <= priority[stack[-1]]:
            result += stack.pop()
        stack.append(i)
        
print(result)