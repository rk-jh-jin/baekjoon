N = int(input())
def findMakerNum(N):
    for i in range(1, 1000000):
        numList = list(str(i))
        if sum(map(int, numList)) + i == N:
            return(i)

    return 0

print(findMakerNum(N))
