from collections import deque

# Adjacency List 길의 갯수가 적은 경우 유리
# Adjacency Matrix 길의 갯수가 많거나 연결 관계를 확인할 때는 이쪽이 유리

def BFS(graph, root):
    visited = []
    queue = deque([root])

    while queue:
        n = queue.popleft()
        if n not in visited:
            visited.append(n)
            queue += graph[n] - set(visited)

    return visited

graph_list = {1: set([3, 4]),
              2: set([3, 4, 5]),
              3: set([1, 5]),
              4: set([1]),
              5: set([2, 6]),
              6: set([3, 5])}
root_node = 1

print(BFS(graph_list, root_node))