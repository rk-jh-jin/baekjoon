import sys
import copy
input = sys.stdin.readline
N = int(input())
field = [list(map(int, input().split())) for i in range(N)]
moves = [[-1, 0], [1, 0], [0, 1], [0, -1]]
R = max(max(field))

def dfs(field, start, CR):
    dq = [start]
    while dq:
        x, y = dq.pop()
        field[x][y] = 0
        for move in moves:
            new_x, new_y = x + move[0], y + move[1]
            if 0 <= new_x < N and 0 <= new_y < N and CR < field[new_x][new_y]:
                dq.insert(0, (new_x, new_y))
                field[new_x][new_y] = 0
    return 1

result = 1
for k in range(1, R):
    count = 0
    copy_field = copy.deepcopy(field)
    for i in range(N):
        for j in range(N):
            if k < copy_field[i][j]:
                count += dfs(copy_field, (i, j), k)
    result = max(result, count)

print(result)