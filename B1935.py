import string

N = int(input())
case = input()
number = {}
for i in range(N):
    number[string.ascii_uppercase[i]] = input()

operator = ['*', '/', '+', '-']

stack = []
result = ''
idx = 0
for i in case:
    cal = ''
    #기호 나올 때까지 스택 쌓기
    if i in operator:
        cal = i + stack.pop()
        cal = stack.pop() + cal

        stack.append(str(eval(cal)))
    else:
        stack.append(number[i])

result = float(stack[0])
print('%.2f' %result)