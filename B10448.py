triCase = [int((n * (n + 1)) / 2) for n in range(1, 45)]

N = int(input())
case = [int(input()) for i in range(N)]

for num in case:
    count = 0
    for a in triCase:
        for b in triCase:
            if num - (a + b) in triCase:
                count = 3

    if count == 0:
        print(0)

    else:
        print(1)
                


