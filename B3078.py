N, K = map(int, input().split())
name = [0] * 21
student = [0] * N
count = 0

for i in range(N):
    l = len(input())
    student[i] = l
    if i > K:
        name[student[i - K - 1]] -= 1
    count += name[l]
    name[l] += 1

print(count)