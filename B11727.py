N = int(input())
first = 1
second = 3
temp = 0

for i in range(N - 1):
    temp = first
    first = second
    second = (temp * 2 + first) % 10007

print(first)