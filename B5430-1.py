from collections import deque
import ast
import sys
input = sys.stdin.readline

T = int(input())
for i in range(T):
    order = input()
    N = int(input())
    arr = deque(ast.literal_eval(input()))
    Dcount = order.count('D')
    isRev = False

    if N < Dcount or (arr == [] and Dcount > 0):
        print('error')
        continue
    elif N == Dcount or (arr == [] and Dcount == 0):
        print([])
        continue

    for j in order:
        if j == 'R':
            isRev = not isRev
        if j == 'D':
            if isRev:
                arr.pop()
            else:
                arr.popleft()
    
    if isRev:
        arr.reverse()

    print(str(list(arr)).replace(' ', ''))
