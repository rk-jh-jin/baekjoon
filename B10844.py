N = int(input())

numCase = [[1 for i in range(10)] for i in range(N)]
numCase[0][0] = 0
for i in range(1, N):
    for j in range(10):
        if j == 0:
            numCase[i][j] = numCase[i - 1][j + 1]

        elif j == 9:
            numCase[i][j] = numCase[i - 1][j - 1]

        else:
            numCase[i][j] = numCase[i - 1][j - 1] + numCase[i - 1][j + 1]

print(sum(numCase[N - 1]) % 1000000000)